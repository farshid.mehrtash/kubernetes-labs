# Kubernetes labs

![](assets/images/kubernetes-logo-4-small.png)

## Lab Environment

Instructor will provide access to lab environment

## Labs

### 1. Install and Setup

* 1.1 : [Setup a single node kubernetes cluster with minikube](install-and-setup/1-minikube.md) (Optional)
* 1.2 : [Setup a multi node kubernetes cluster ](install-and-setup/2-kubernetes-cluster-setup.md) (Optional)
* 1.3 : [Use EKS](install-and-setup/eks.md)

### 2. Exploring Kubernetes

* 2.1: [Get familiar with kubectl command](exploring/1-kubectl.md)
* 2.2: [Setup a dashboard](exploring/2-dashboard.md)

### 3. Pods

* 3.1: [Running a pod](pods/1-pod-run/README.md)
* 3.2: [Deploying pods via manifest](pods/2-pod-manifest/README.md)

### 4. ReplicaSet

* 4.1: [Deploy a ReplicaSet](replicaset/nginx/README.md)

### 5. DaemonSet

* 5.1: [Deploy a DaemonSet](daemonset/README.md)

### 6. Resource Limits

* 6.1: [Specify resource limits](resource-limits/README.md)

### 7. Deployment

* 7.1: [Basic deployment](deployments/basic/README.md) - Deploy multiple replicas of nginx
* 7.2: [Rollout deployment](deployments/rollout/README.md)
* 7.3: [Blue/Green deployment](deployments/blue-green/README.md)
* 7.4: [Canary deployment and A/B deployment](deployments/canary/README.md)

### 8. Services

* 8.1: [Deploy a service](services/nginx/README.md)

### 9. Networking

* 9.1: [Custom DNS settings](networking/dns/DNS.md)

### 10. Labels

* 10.1: [Labeling Pods](labels/pod-labels/README.md)
* 10.2: [Pod placement](labels/pod-placement/README.md)

### 11. Configurations

* 11.1: [Create and use config maps](config-map/README.md)
* 11.2: [Using secrets](secret/README.md)

### 12. Volumes

* 12.1: [Using EmptyDir scratch space](volumes/shared-volume-empty/README.md)
* 12.2: [Sharing a persistent volume](volumes/pv-shared/README.md)
* 12.3: [Using shared volume as nginx storage](volumes/pv-nginx/README.md)

### 13. Sidecar

* 13.1: [Sidecar usage](sidecar/README.md)

### 14. Metrics

* 14.1: [Examining Kubernetes metrics](metric-server/README.md)

### 15. Auto Scale

* 15.1: [Autoscaling based on resource usage](autoscale/README.md)

### 16. Ingress and Load balancing

* 16.1 : Setup Ingress controller (TODO)

### 17. Practice Labs

* 17.1: [Bundle and deploy a jokes server](practice-labs/joke-server/README.md)
* 17.2: [Deploy Pacman and Mario video games!](practice-labs/games/README.md)
* 17.3: [Deploy a wordpress application](practice-labs/wordpress/README.md)
* 17.4: [Deploy RedMine application](practice-labs/redmine/README.md)