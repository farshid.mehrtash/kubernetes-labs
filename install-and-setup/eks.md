# Setup kubectl on Ubuntu to interact with EKS

## Overview

We will setup `awscli` and `kubectl` on Ubuntu to interact with EKS cluster.

## Duration

30-45 minutes

## Steps

### Step 1 : Login to Machines

Each student will have 1 lab machine and 1 EKS cluster

Login details and config file will be provided.

### Step 2: Install awscli

In order to interact with AWS, we need to install `awscli` on our machine.

Note: This will install awscli version 2

```bash
sudo curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
sudo unzip awscliv2.zip
sudo ./aws/install
aws --version
```

Output should be something like

```console
aws-cli/2.2.4 Python/3.8.8 Linux/5.4.0-1045-aws exe/x86_64.ubuntu.20 prompt/off
```

### Step 3: Install kubectl

`kubectl` is the command line tool to interact with Kubernetes cluster.

We will install the latest version of `kubectl`

```bash
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```

Check version

```bash
kubectl version --client
```

Output should be something like

```console
Client Version: version.Info{Major:"1", Minor:"28", GitVersion:"v1.28.1", GitCommit:"632ed300f2c34f6d6d15ca4cef3d3c7073412212", GitTreeState:"clean", BuildDate:"2021-08-19T15:45:37Z", GoVersion:"go1.16.7", Compiler:"gc", Platform:"linux/amd64"}
```

### Step 4: Configure awscli

We need to configure awscli with our AWS credentials.

```bash
aws configure
```

Instructor will provide the AWS credentials.

### Step 5: Configure kubectl

We need to configure kubectl with our EKS cluster.

```bash
aws eks update-kubeconfig --name <cluster-name>
```

Instructor will provide the cluster name.

### Step 6: Verify

```bash
kubectl get nodes
```

Output should be something like

```console
NAME                                         STATUS   ROLES    AGE   VERSION
ip-10-0-1-101.us-west-2.compute.internal     Ready    <none>   10m   v1.21.2-eks-55daa9d
```

## Conclusion

By following the provided steps, you have now set up `awscli` and `kubectl` on Ubuntu to interact with EKS. This ensures efficient management of the AWS EKS environment. Ensure your AWS credentials are secure and `kubectl` configurations align with your EKS cluster. You're now ready for seamless EKS
cluster management!